# ILS-validation.jl


**ILS-validation.jl** is a julia package for the validation of the data obtained through a inter-laboratory study. The code was developed and tested in julia version 1.5.1. 
All laboratories in this studie obtained samples spiked with a set of internal standards. This code checks the presence of these internal standards in the raw LC-HRMS data (.mzXML files). 


## Installation

Given that **ILS-validation.jl** is not a registered julia package, for installation the *url* to the repository and the julia package manager are necessary.

```julia
using Pkg
Pkg.add(PackageSpec(url="https://bitbucket.org/Denice_van_Herwerden/ils-validation/src/main/"))


```

## Dependencies
To be able to use this package the [**MS_Import.jl**](https://bitbucket.org/SSamanipour/ms_import.jl/src/master/) package need to be installed.


## Usage

The ILS-validation.jl algorihtm can be used by the following command line:

```julia
using ILS-validation

is_dataframe = referenceCheck("Inputs")

is_dataframe = referenceCheck(path2files::String, path2ref::String, imp::Bool)

```


### Inputs
The ILS-validation algorithm requires 3 input parameters. The second parameter (path2ref) should be "" (an empty string) if the user wants to use the default reference file. 
In case not all files were processes (e.g. due to a out of memory error) the imp parameter can be set to 1 to resume processing the files where it was stopped.

```julia
path2files::String = "path2files" # is the path to the .mzXML files.
path2ref::String = "" # is tha path to the reference file containing the information on the internal standards. use "" for default reference file
imp::Bool = 0 # is used whether a new output file will be generated (0) or a existing one should be loaded (1).

```

**Reference file example**:

|Name|	polarity|	IS|	formula	M|	M+H|
|----|----------|---------|--------------|---------|
|Caffeine 13C3| 	pos|	1|	C5H10N4O2(13C)3|	197.0904|	198.09768|
|Nicotine-D4|	pos|	1|		166.140805|	167.148081|
|Cotinine D3| 	pos|	1|	C10H9D3N2O|	179.1138|	180.12108|
|Simazine-D10|	pos|	1|		211.140891|	212.148167|
|Carbamazepine D10| 	pos|	1|	C15H2D10N2O|	246.1577|	247.16498|
|Diuron-D6|	pos|	1|		238.054679|	239.061955|



### Outputs

```julia

is_dataframe::DataFrame# is a dataframe containing the file names and mass, median mass, retention time and intensity of the internal standards defined in the reference file.


```

Additionally, **ILS-validation** saves the generated dataframe as a ".csv" file in the location defined by parameter dirname("path2files"). 
An example of this dataframe for 3 files and 1 reference compounds is provided below. 
For each compound in the reference file the mass at the apex (mass), median mass at the apex for the three scans around the apex (median), retention time at the apex (rt) and intensity of the apex (int) are recorded.

|filename|	Caffeine 13C3  mass|	Caffeine 13C3  median|	Caffeine 13C3  rt|	Caffeine 13C3  int|
|--------|-------------------------|-------------------------|-------------------|------------------------|
|02_001_pd.mzXML|	198.0969543|	198.09729|	1.89|	11724|
|02_121_pd.mzXML|	198.0966797|	198.0969238|	1.89|	9340|
|02_141_pd.mzXML|	198.0973663|	198.0975342|	1.91|	8836|




## Contribution

Issues and pull requests are welcome! Please contact the developers for further information.