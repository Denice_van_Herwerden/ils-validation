#Load packages
using MS_Import
using CSV
using DataFrames
using Plots
using Statistics
path2dir = @__DIR__
include(path2dir*"\\MS_Visualization.jl")


########## Functions ############

function referenceCheck(path2files,path2ref,imp)
	#Import reference data
	if path2ref == ""
		path2ref = @__DIR__
	end
	ref = CSV.read(path2ref*"\\Ref.csv",DataFrame)
	#for each folder containing mzXML files
		files = readdir(path2files)

		#build internal standard dataframe/import if already available
		if imp == 1
			if isfile(path2files*"\\"*basename(path2files)*" output.csv")
				is_dataframe = CSV.read(dirname(path2files)*"\\"*basename(path2files)*" output.csv",DataFrame)
			else
				println("Path to existing output file not found")
				println(path2files*"\\"*basename(path2files)*" output.csv")
			end
		else
			is_dataframe = DataFrame()
			is_dataframe[!,"filename"] = fill("",length(files))
			for r = 1:size(ref,1)
				is_dataframe[!,ref[!,"Name"][r]*" mass"] = fill(NaN,length(files))
				is_dataframe[!,ref[!,"Name"][r]*" median"] = fill(NaN,length(files))
				is_dataframe[!,ref[!,"Name"][r]*" rt"] = fill(NaN,length(files))
				is_dataframe[!,ref[!,"Name"][r]*" int"] = fill(NaN,length(files))
			end
		end

		#check existance of directory for saving figures
		if ~isdir(path2files*"\\"*basename(path2files)*" figs")
			mkdir(path2files*"\\"*basename(path2files)*" figs")
		end
		#for each mzXML file
		for f = 1:length(files)
			#skip files that were already evaluated in case a previous output file was loaded
			if imp == 1 && !isempty(is_dataframe[!,"filename"][f]) && (isfile(path2files*"\\"*basename(path2files)*" figs\\"*files[f][1:end-6]*" 6 disqualified.png") || isfile(path2files*"\\"*basename(path2files)*" figs\\"*files[f][1:end-6]*" 6.png"))
				continue
			end
			#section used for skipping specific files that were already processed
#			if m == 1 && any(files[f][1:2] .== [])
# 				continue
#			elseif m == 1 && any(files[f][1:5] .== [])
#				continue
#			elseif m == 2 && any(files[f][1:2] .== [])
#	 			continue
#			elseif m == 2 && any(files[f][1:5] .== [])
#				continue
#			end

			println()
			println("File "*files[f]*" is beeing processed")
			is_dataframe[!,"filename"][f] = files[f]
			mzXML = import_files(path2files,[files[f]],[minimum(ref[!,"M+H"])-30,maximum(ref[!,"M+H"])+30])
			#for each reference compound
			for r = 1:size(ref,1)
				pion = ref[!,"M+H"][r]
				tol = 0.010
				thresh = 300
				MS1val = mzXML["MS1"]["Mz_values"]
				MS1int = mzXML["MS1"]["Mz_intensity"]
				#set all values outside the tolerance range to 0
				MS1int = MS1int .* ((MS1val .>= (pion - tol)) .& (MS1val .<= (pion + tol)))
				MS1val = MS1val .* ((MS1val .>= (pion - tol)) .& (MS1val .<= (pion + tol)))
				max = argmax(MS1int)
				maxr = argmax(MS1int[max[1]+1,:])
				if max[1] == 1
					maxl = Inf
				else
					maxl = argmax(MS1int[max[1]-1,:])
				end
				#check if current/neighboring scans are above thresh
				if (MS1int[max] <= thresh) ||  maxl == Inf || ((MS1int[max[1]+1,maxr] <= thresh) && (MS1val[max[1]-1,maxl] <= thresh))
					#only plot
	    			opt = "Base"
	    			scan_lim = [0,0]
					P = plot()
					XIC, P = MS_Visualization.XIC_Extract_MS1(0,mzXML,ref[!,"M+H"][r],opt,tol,scan_lim)
					P = plot!(mzXML["MS1"]["Rt"],XIC,xlabel="Retention time (min)",ylabel="Intensity",title="The Base peak",label=string(pion)*" pm "*string(tol)*" MS1",dpi=300)
					xlims!(scan_lim[1],maximum(mzXML["MS1"]["Rt"]))
					display(P)
					savefig(path2files*"\\"*basename(path2files)*" figs\\"*files[f][1:end-6]*" $r disqualified.png")
				else
					#save information to is_dataframe
					is_dataframe[!,ref[!,"Name"][r]*" mass"][f] = MS1val[max]
					is_dataframe[!,ref[!,"Name"][r]*" median"][f] = median([MS1val[max] MS1val[max[1]+1,maxr] MS1val[max[1]-1,maxl]])
					is_dataframe[!,ref[!,"Name"][r]*" int"][f] = MS1int[max]
					is_dataframe[!,ref[!,"Name"][r]*" rt"][f] = mzXML["MS1"]["Rt"][max[1],1]
					if any(files[f][1:2] .== ["16"])
					else
						continue
					end
					#plot
		    		opt = "Base"
		    		scan_lim = [0,0]
					P = plot()
					XIC, P = MS_Visualization.XIC_Extract_MS1(0,mzXML,ref[!,"M+H"][r],opt,tol,scan_lim)
					P = plot!(mzXML["MS1"]["Rt"],XIC,xlabel="Retention time (min)",ylabel="Intensity",title="The Base peak",label=string(pion)*" pm "*string(tol)*" MS1",dpi=300)
					xlims!(scan_lim[1],maximum(mzXML["MS1"]["Rt"]))
					display(P)
					savefig(path2files*"\\"*basename(path2files)*" figs\\"*files[f][1:end-6]*" $r.png")
				end
			end
			#save results
			CSV.write(dirname(path2files)*"\\"*basename(path2files)*" output.csv",is_dataframe)
		end
	return is_dataframe
end


########## Example ############


#path to folders containing mzXML files
path2files = "C:\\Users\\dherwer\\OneDrive - UvA\\Post processing\\CMS\\Datanote files\\mzXML files\\Predefined method"
#path to reference file containing internal standard information
#"" is used for default reference file that comes with the package
path2ref = ""
#imp == 0 a new data structure will be made. imp == 1 a previously saved data structure
#will be imported and only the missing information will be obtained.
imp = 0

is_dataframe = referenceCheck(path2files,path2ref,imp)
