module MS_Visualization


push!(LOAD_PATH,pwd())

using PyCall
using Plots


using MS_Import


export XIC_Extract_MS1

###############################################################################
# Parameter space for testing
#numchan=1
#namechan=["MS1","MS2"]
#pathin="/home/saer/Desktop/juliaMS_data_tests"
#filenames=["180420-SWATH-JO054-006 Day5 C.mzXML"]
#format="mzxml"
#mz_thresh=600

# chrom=import_files(format,numchan,namechan,pathin,filenames,mz_thresh)

#####################
#vis=1
#mass=237.1022
#tol=0.01
#opt="Base" #"TIC"
#scan_lim=[325,350] #""

#XIC,P=XIC_Extract_MS1(vis,chrom,mass,opt,tol,scan_lim)
#plot(XIC,label="XIC for the mass 239.0623",dpi=300)
#xlims!(900,1100)
#ylims!(0,2000)
#xlabel!("Scan number")
#ylabel!("Intensity")
#savefig( "D:\\Data\\julia\\data_jake\\Figures_Pub\\XIC_diuron.png")

#i=1000
###############################################################################
# XIC extract


function XIC_Extract_MS1(vis,chrom,mass,opt,tol,scan_lim)

    Mass_vect=chrom["MS1"]["Mz_values"]
    Int_vect=chrom["MS1"]["Mz_intensity"]
    time = chrom["MS1"]["Rt"]
    if vis==1 && opt=="Base"
        XIC=zeros(size(Int_vect,1),1)
        for i=1:size(Int_vect,1)
            tv1=Int_vect[i,findall(x -> mass+tol > x >= mass-tol,Mass_vect[i,:])]
            if (length(tv1)>0)
                XIC[i] = maximum(tv1)
            end
        end
        P=plot!(1:length(XIC),XIC,xlabel="Scan number",ylabel="Intensity",
        title="The Base peak",label=string(mass)*" pm "*string(tol)*" MS1",dpi=300);
        if scan_lim[2] == 0
            xlims!(scan_lim[1],length(XIC))
        else
            xlims!(scan_lim[1],scan_lim[2])
        end
#        display(P);

    elseif (vis==1 && opt=="TIC")
        XIC=zeros(size(Int_vect,1),1)
        for i=1:size(Int_vect,1)
            tv1=Int_vect[i,findall(x -> mass+tol > x >= mass-tol,Mass_vect[i,:])]
            if (length(tv1)>0)
                XIC[i] = sum(tv1)
            end
        end
        P=plot!(XIC,xlabel="Scan Number",ylabel="Intensity",
        title="The TIC",label=string(mass)*" pm "*string(tol)*" MS1",dpi=300);
        if length(scan_lim)>0
            xlims!(scan_lim[1],scan_lim[2])
        end
#        display(P);


    elseif (vis==0 && opt=="Base")
        XIC=zeros(size(Int_vect,1),1)
        for i=1:size(Int_vect,1)
            tv1=Int_vect[i,findall(x -> mass+tol > x >= mass-tol,Mass_vect[i,:])]
            if (length(tv1)>0)
                XIC[i] = maximum(tv1)
            end
        end
        P=[]

    elseif (vis==0 && opt=="TIC")
        XIC=zeros(size(Int_vect,1),1)
        for i=1:size(Int_vect,1)
            tv1=Int_vect[i,findall(x -> mass+tol > x >= mass-tol,Mass_vect[i,:])]
            if (length(tv1)>0)
                XIC[i] = sum(tv1)
            end
        end
        P=[]
    end

    return(XIC,P)

end



end #end of the module
